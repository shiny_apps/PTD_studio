# docker image aguida/shiny-ptd
# 
# base image that contains PTD + shiny.
# This image doesn't contain PTD_studio yet. For bulding PTD studio, please refer 
# to Dockerfile_ptdstudio
#
# interactive terminal usage
# ------------------------------
#   docker run -it aguida/shiny-ptd R
#
# to build the container
# ------------------------------
#   sudo docker build -t aguida/shiny-ptd .
#
# to run it
# ------------------------------
#   docker run -p 3838:3838 shiny-ptd
# ########

FROM r-base:3.5.0

MAINTAINER Alessandro Guida, PhD "alessandro.guida@ieo.it"

# APPEND to list file
#RUN echo "deb https://cloud.r-project.org/bin/linux/ubuntu xenial-cran35/" >> /etc/apt/sources.list

RUN apt-get update && apt-get install -y \
    sudo \
    pandoc \
    pandoc-citeproc \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libssh2-1-dev \
    #libssl1.0.0 \
    git \
    gcc \
    libxml2-dev \ 
    r-base \
    r-base-dev
    

# Download and install ShinyServer (latest version)
#RUN wget --no-verbose https://s3.amazonaws.com/rstudio-shiny-server-os-build/ubuntu-12.04/x86_64/VERSION -O "version.txt" && \
#    VERSION=$(cat version.txt)  && \
#    wget --no-verbose "https://s3.amazonaws.com/rstudio-shiny-server-os-build/ubuntu-12.04/x86_64/shiny-server-$VERSION-amd64.deb" -O ss-latest.deb && \
#    gdebi -n ss-latest.deb && \
#    rm -f version.txt ss-latest.deb    
    
# packages needed for basic shiny functionality
RUN R -e "install.packages(c('shiny', 'shinydashboard', 'rmarkdown'), repos='https://cloud.r-project.org')"

# Copy configuration files into the Docker image
#COPY shiny-server.conf  /etc/shiny-server/shiny-server.conf
#COPY /app /srv/shiny-server/    

# packages needed for installing PTD
RUN R -e "install.packages(c('devtools', 'XML'), repos='https://cloud.r-project.org')"

#RUN R -e "source('https://bioconductor.org/biocLite.R'); library(BiocInstaller); biocLite(c('cgdsr','parallel','stringr', 'reshape2','data.table','RColorBrewer', 'BiocParallel','magrittr','biomaRt', 'XML','httr','jsonlite', 'ggplot2','ggrepel','grid','S4Vectors','IRanges','GenomicRanges','googleVis','shiny','shinyBS','DT','brglm','BiocStyle','knitr','rmarkdown','dplyr', 'testthat'), dep = TRUE); biocLite('LowMACAAnnotation')"

# get and install PTD
RUN R -e "devtools::install_github('alessando-guida/PrecisionTrialDrawer')"

