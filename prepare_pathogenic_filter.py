# SCRIPT TO GENERATE THE COSMIC DATASET 
# ========================================================================================
#
# Description
# 
# One of the options for the shiny app PTD_Studio, is to filter the mutations by 
# pathogenic alterations. In order to do so, we need to download all the cosmic 
# alterations, filter them and keep only the pathogenic ones, and format the file so that 
# it can be easily imported into PTD_Studio. Ideally we want to keep it as small as 
# lightweight as possible
#
# Requirements
# 
#  -Python3
# ----------------------------------------------------------------------------------------
import pandas as pd
import numpy as np


# DOWNLOAD AND IMPORT COSCMIC DATA
# ----------------------------------------------------------------------------------------
# import the latest downloaded dataset from COSMIC SFTP SERVER
# /files/grch37/cosmic/v83/CosmicGenomeScreensMutantExport.tsv.gz
#
# CosmicGenomeScreensMutantExport
# A tab separated table of the complete curated COSMIC dataset (targeted screens) from 
# the current release. It includes all coding point mutations, and the negative data set. 
#
# unzip the file into CosmicGenomeScreensMutantExport.tsv
#
# Load the file in python
print("loading cosmic file")
df = pd.read_table("/Users/ale/IEO/projects/ACC-bioinformatics/Acc_lung_project/acclung_ion_reanalysis/reanalysis_october2017/hotspot_file/CosmicGenomeScreensMutantExport.tsv", sep="\t")


# FILTER OUT NON-PATHOGENIC MUTATIONS
# ----------------------------------------------------------------------------------------
# sing the FATHMM field, we select only those cosmic mutations annotated to be pathogenic
# in all tumor types. We could extend here and add an extra filter tumor specific but we 
# decided not to do it and to keep it general.
print("Filtering Pathogenic mutations")

# Filter pathogenic mutations
df_patho = df[df['FATHMM prediction'] == "PATHOGENIC"]

# print some statistics
n_left = (df_patho.shape[0]/df.shape[0])*100
print("Percentage of variants kept: %s" % round(n_left,2))


# CLEAN UP THE DATAFRAME FROM NOT RELEVANT INFO
# ----------------------------------------------------------------------------------------
# extract genenames and Mutation AA
print("Clean up dataframe")
df_clean = pd.DataFrame(df_patho[["Gene name", "Mutation AA"]].reset_index(drop=True))

# EXPORT file
# ----------------------------------------------------------------------------------------
print("Exporting as cosmic_v83_pathogens.tab into data subfolder ")
df_clean.to_csv("data/cosmic_v83_pathogens.tab", sep="\t", index=False, header=False)
