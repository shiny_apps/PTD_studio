# MODULE DESCRIPTION - SERVER SIDE
# ==============================================================================
# This module is design to contain all the Server function to generate
# the plot in the main frame of the shiny app FOR THE SURVIVAL ANALYSIS
# 
# This module contains the button logic to display the PLOT it self, not the 
# interface to control the plot behaviour. 
# ==============================================================================

flog.info("Loading Module: module_plot_survival_SERVER.R")


# SERVER  ----------------------------------------------------------------------
plotSurvivalBtnSERVER <- function(input, output, session
                                  , PANEL
                                  , GROUP_bln=TRUE
                                  , FUN = survPowerSampleSize
                                  , ...) 
{
  
  # Function Description
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # This server function handles the backend of the action button. It takes as 
  # arguments: 
  #
  # PANEL 
  #     which is the newCancerPanel PTD object used to run the simulation
  # FUN
  #     which is the plotting function (coveragePlot, saturationPlot, etc..)
  # ... 
  #     which allows to customize the plotting function directly on the app layer
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  flog.info("\tLoading Module function: plotSurvivalBtn()")
  
  observeEvent(input$plotSurvivalBtn, 
               {
                 
                 flog.info("(Survival) Plot button triggered >>>>>>>>>>>>>")
                 
                 if(is.null(PANEL$data)){
                   flog.error("Trying to display Error message from button push")
                   showNotification("Error: You need to upload the edited template before running the simulation"
                                    , duration = 5
                                    , closeButton =  TRUE
                                    , type = "error"
                   )
                   return(NULL) # this prevents the plot to be shown if panel is not defined
                 } 
                 
                 # Init parameter list
                 
                 
                 # Handle differnt  type of functions
                 
                 # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                 # survPowerSampleSize
                 # -------------------------------------------------------------
                 if(FUN == "survPowerSampleSize"){
                   
                   flog.info("(Survival) Plot: survPowerSampleSize()")
                   
                   
                   # DEFINE PARAMETERS
                   # ------------------------------------------
                   # add parameters to the parameter variable
                   parameters_lst <- list(
                                            HR = isolate(input$HR)
                                          , power = isolate(input$power)
                                          , alpha = isolate(input$alpha)
                                          , ber = isolate(input$ber)
                                          , fu = isolate(input$fu)
                                          , case.fraction = isolate(input$case.fraction)
                    )

                   # Debuggint plots
                   # ------------------------------------------
                   flog.debug(paste0("(Survival) HR:"
                                     , toString(isolate(input$HR))))
                   flog.debug(paste0("(Survival) Power:"
                                     , toString(isolate(input$power))))
                   flog.debug(paste0("(Survival) alpha:"
                                     , toString(isolate(input$alpha))))
                   flog.debug(paste0("(Survival) Ber - base event rate:"
                                     , toString(isolate(input$ber))))
                   flog.debug(paste0("(Survival) fu - follow up:"
                                     , toString(isolate(input$fu))))
                   flog.debug(paste0("(Survival) Case.fraction:"
                                     , toString(isolate(input$case.fraction))))
                   
                   # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                   # survPowerSampleSize
                   # -------------------------------------------------------------
                  } else if (FUN == "survPowerSampleSize1Arm"){ 
                     
                     flog.info("(Survival) Plot: survPowerSampleSize1Arm()")
                     
                     
                     # DEFINE PARAMETERS
                     # ------------------------------------------
                     # add parameters to the parameter variable
                     parameters_lst <- list(
                         power = isolate(input$power)
                       , alpha = isolate(input$alpha)
                       , fu = isolate(input$fu)
                       , MED0 = isolate(input$MED0)
                       , MED1 = isolate(input$MED1)
                       
                     )
                     
                     # Debuggint plots
                     # ------------------------------------------
                     flog.debug(paste0("(Survival) Power:"
                                       , toString(isolate(input$power))))
                     flog.debug(paste0("(Survival) alpha:"
                                       , toString(isolate(input$alpha))))
                     flog.debug(paste0("(Survival) fu - follow up:"
                                       , toString(isolate(input$fu))))
                     flog.debug(paste0("(Survival) MED0:"
                                       , toString(isolate(input$MED0))))
                     flog.debug(paste0("(Survival) MED1:"
                                       , toString(isolate(input$MED1))))
                     
                  } else if (FUN == "propPowerSampleSize"){ 
                    
                    flog.info("(Survival) Plot: propPowerSampleSize()")
                    
                    
                    # DEFINE PARAMETERS
                    # ------------------------------------------
                    # add parameters to the parameter variable
                    parameters_lst <- list(
                      power = isolate(input$power)
                      , alpha = isolate(input$alpha)
                      , case.fraction = isolate(input$case.fraction)
                      , pCase = isolate(input$pCase)
                      , pControl = isolate(input$pControl)
                      
                    )
                    
                    # Debuggint plots
                    # ------------------------------------------
                    flog.debug(paste0("(Survival) Power:"
                                      , toString(isolate(input$power))))
                    flog.debug(paste0("(Survival) alpha:"
                                      , toString(isolate(input$alpha))))
                    flog.debug(paste0("(Survival) case.fraction"
                                      , toString(isolate(input$case.fraction))))
                    flog.debug(paste0("(Survival) pCase:"
                                      , toString(isolate(input$pCase))))
                    flog.debug(paste0("(Survival) pControl:"
                                      , toString(isolate(input$pControl))))
                   
                   
                 } else {
                   stop("something went wrong. The function name provided for the power analysis doesn't not exist")
                 }
                 
                 # Prepare grouping variables
                 # ------------------------------------------
                 # Convert "None" to NA:
                 # "None" is easier to understand than "NA" but PTD needs "NA" as input
                 grouping_var <- isolate(GENERAL_PLOT_SELECTION$groups)
                 grouping_var  <- gsub("None", NA, grouping_var)
                 
                 # Debuggint plots
                 # ------------------------------------------
                 flog.debug(paste0("(Survival) Alteration:"
                                   , toString(isolate(GENERAL_PLOT_SELECTION$alterations))))
                 flog.debug(paste0("(Survival) Tumor types:"
                                   , toString(isolate(GENERAL_PLOT_SELECTION$tumor_types))))
                 flog.debug(paste0("(Survival) Grouping choices:"
                                   , toString(isolate(grouping_var))))
                 flog.debug(paste0("(Survival) Pathogenic filter:"
                                   , toString(isolate(PATHO_FILTER$status))))
                 
                 # Prepare parameters for the function 
                 # ------------------------------------------
                 parameters_lst <- list(isolate(PANEL$data)
                                        , alterationType = isolate(GENERAL_PLOT_SELECTION$alterations)
                                        , tumor_type = isolate(GENERAL_PLOT_SELECTION$tumor_types)
                                        , tumor.weights = isolate(TUMOR_TYPE_WEIGHTS$weights())[isolate(GENERAL_PLOT_SELECTION$tumor_types)]
                 ) %>% c(., parameters_lst) 
                                        
                 
                 
                 # add Grouping parameter unless specified otherwise
                 # in survival plot, the "grouping var"  is preplaced with "var"
                 #if(GROUP_bln){parameters_lst <- c( parameters_lst, grouping=grouping_var)}
                 if(GROUP_bln){parameters_lst <- c( parameters_lst, var=grouping_var)}
                 
                 # Preview extra parameters
                 # ------------------------------------------
                 flog.debug("(Survival) show all the other parameters (...):")
                 print(list(...))
                 
                 # Call Main plotting function
                 # ------------------------------------------
                 output$survival_plot <- renderPlot({
                   withCallingHandlers(
                     {
                       flog.debug("(Survival) Updating Plot/Table")
                     
                     
                       # Render Plot plot
                       # ~~~~~~~~~~~~~~~~
                         flog.debug("(Survival) Rendering Plot")
                         do.call(FUN, c(parameters_lst, ...))
                       
                     } , error = function(e){
                       flog.error(e)
                       # show error message in the shiny console space
                       showNotification(e$message, duration = 5, closeButton =  TRUE, type = "error")
                     } , warning = function(e){
                       flog.warn(e)
                       # show error message in the shiny console space
                       showNotification(e$message, duration = 5, closeButton =  TRUE, type = "warning")
                     } , message = function(e){
                       flog.info(e)
                       # show error message in the shiny console space
                       showNotification(e$message, duration = 5, closeButton =  TRUE, type = "message")
                     }
                   )
                 })

                 # Call Main Table rendering function
                 # ------------------------------------------
                 withCallingHandlers({
                     flog.debug("(Survival) Updating Plot/Table")
                     
                     # Render Table
                     # ~~~~~~~~~~~~~~~~
                     flog.debug("(Survival) Rendering Table")
                     # generate table
                     mytable <- do.call(FUN, c(parameters_lst, noPlot=TRUE, ...))
                     # output table
                     output$survival_table <- DT::renderDataTable(mytable)
                     
                     # Sys.sleep(0.2)
                     # setProgress(1)
                     #})
                     
                   } , error = function(e){
                     flog.error(e)
                     # show error message in the shiny console space
                     showNotification(e$message, duration = 5, closeButton =  TRUE, type = "error")
                   } , warning = function(e){
                     flog.warn(e)
                     # show error message in the shiny console space
                     showNotification(e$message, duration = 5, closeButton =  TRUE, type = "warning")
                   } , message = function(e){
                     flog.info(e)
                     # show error message in the shiny console space
                     showNotification(e$message, duration = 5, closeButton =  TRUE, type = "message")
                   }
                 )
                 
                 flog.debug("(Survival) Plot and Table completed <<<<<<<<<<<<<")
               }
  )
}