
How to Install a Shiny serve on a Ubuntu Virtual machine on AWS
================================================================================

While this is not a guide to how to launch a EC2 AWS virtual machine, I have taken a few notes about how I was able to get the shiny app to run smoothly on a Ubuntu virtual machine running as a EC2 instance. I will therefore assume that the reader was able to lauch the instance, enable port 22 for ssh connection and log in through ssh. 


Install shiny server
--------------------------------------------------------------------------------

```
# install R
sudo apt install r-base-core
```

```
# install shiny and rstudio
sudo su - -c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""

# install shiny server
# ---------------------
sudo apt-get install gdebi-core
# Make sure it is the lastest version !!!! Download it
wget https://download3.rstudio.org/ubuntu-12.04/x86_64/shiny-server-1.5.5.872-amd64.deb
# install it
sudo gdebi shiny-server-1.5.5.872-amd64.deb
```

The shiny is launched on the port 3838 by default. Make sure that it is open in AWS.

Configure the server
--------------------------------------------------------------------------------

shiny server configurations are available at

*  ```/etc/shiny-server/shiny-server.conf```

to restart after making changes

```
sudo systemctl start shiny-server
sudo systemctl restart shiny-server
```

* Log files will be stored here: ```/var/log/shiny-server```
* The path for loading the apps is here ```/srv/shiny-server/```


```
usermod -aG shiny ubuntu
```

restart the terminal to see the changes with ```$ groups```.

Add Swap memory
--------------------------------

Since version 0.0.7, the cosmic filteration step requires more RAM memory that is availabe causing the app to crash. I will therefore try to add some swap memory in the attemp to deal with this issue.

```
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo chmod 600 /var/swap.1
sudo /sbin/swapon /var/swap.1
```

To enable it by default after reboot, add this line to /etc/fstab:

```
/var/swap.1   swap    swap    defaults        0   0
```

* source: [https://stackoverflow.com/questions/17173972/how-do-you-add-swap-to-an-ec2-instance](https://stackoverflow.com/questions/17173972/how-do-you-add-swap-to-an-ec2-instance)

Extra
-------------------------------

to also have rmarkdown compatibility install

```{r}
install.packages("rmarkdown")
```