PTD_Studio
================================================================================
  
Shiny app for the PrescriptionTrialDrawer package. For more information please refer to [gmelloni.github.io/ptd](gmelloni.github.io/ptd)


About this repository
--------------------------------------------------------------------------------

 * **Production** brach: dedicated for the stable running version of the program 
 * **Master** branch is the main brench where to push keep all the development
 
 A demo with the **Production** branch is available at [gmelloni.github.io/ptd/shinyapp.html](gmelloni.github.io/ptd/shinyapp.html)


External files
--------------------------------------------------------------------------------

###  Cosmic pathogenic mutations. 

One of the options for the shiny app PTD_Studio, is to filter the mutations by 
pathogenic alterations. In order to do so, we need to download all the cosmic 
alterations, filter them and keep only the pathogenic ones, and format the file 
so that it can be easily imported into PTD_Studio. Ideally we want to keep it as 
small as lightweight as possible. This can be achieved with the script 
```prepare_pathogenic_filter.py```. This generates the file 
```data/cosmic_v83_pathogens.tab``` which contains the pathogenic mutations NON 
tumor specific that could be used to filter the data input from PTD.


Dependencies
--------------------------------------------------------------------------------

* git lfs [https://git-lfs.github.com/](https://git-lfs.github.com/)

**IMPORTANT**: git lfs needs to be installed before running ```git clone <this repo>``` 

```
# for ubuntu 
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
```

* Main app on which this shiny interface is based on: [PrescriptionTrailDesigner](https://gitlab.com/IEO_Research/R_package_panel_and_projection)

### Dependencies installation

Install dependencies for devtools

```
sudo apt-get install build-essential libcurl4-gnutls-dev libxml2-dev libssl-dev
```

install devtools

```
# install devtools
install.packages("devtools")
# load the library
library(devtools)
```

Install PrecisionTrialDrawer dependency

```
devtools::install_github('gmelloni/PrecisionTrialDrawer')
```



